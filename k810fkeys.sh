#!/bin/bash

# bash version of this program
# http://www.trial-n-error.de/posts/2012/12/31/logitech-k810-keyboard-configurator/k810_conf-v0.1.tar.bz2
# alt
# http://blog.chschmid.com/media/k810_conf-v0.1.tar.bz2


k810_seq_fkeys_on='\x10\xff\x06\x15\x00\x00\x00'
k810_seq_fkeys_off='\x10\xff\x06\x15\x01\x00\x00'


case $1 in
    on) ;;
    off) ;;
    *) echo "Usage: $(basename $0) on|off"
       exit 1
       ;;
esac

if [  "$(whoami)" != "root" ]
then
    echo "Must be run as root (sudo)"
    exit 1
fi

for d in $(ls -1 /sys/class/hidraw/hidraw*/device/uevent)
do
    if [ -n "$(cat $d | grep 'HID_NAME=Logitech K810')" ]
    then
	h=$(echo "${d}" | grep -o 'hidraw[0-9]')
	case $1 in
	    on)
		echo -n -e ${k810_seq_fkeys_on} | dd oflag=nonblock of=/dev/${h} 2> /dev/null
	    ;;
	    off)
		echo -n -e ${k810_seq_fkeys_off} | dd oflag=nonblock of=/dev/${h} 2> /dev/null
	    ;;
	esac
	exit 0
    fi
done

echo "No 'Logitech K810' keyboard found"
