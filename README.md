A small bash script to turn the function keys on a Logitech K810 keyboard on or off.

By default the top row keys on the Logitech K810 keyboard are media keys and
you need to press the FN key to access the F-keys.
This script can invert that behavior.

I did not invent this. This script is just a bash version of a C program that
can be found here:
http://www.trial-n-error.de/posts/2012/12/31/logitech-k810-keyboard-configurator
